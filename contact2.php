<?php
	session_start();

	if (isset($_POST['submit'])) {
		// send email
		$_SESSION['contact-success'] = true;
		header('Location: contact2.php');
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Contact Us</title>
</head>
<body>
	
<h1>Contact Us</h1>

<?php if (isset($_SESSION['contact-success'])) : ?>
	<p>We will get back to you shortly.</p>
	<?php unset($_SESSION['contact-success']) ?>
<?php endif; ?>

<form action="contact2.php" method="post">
	<div>
		Email: <input type="text" name="email">	
	</div>
	<div>
		<textarea name="message" cols="30" rows="10"></textarea>	
	</div>
	<div>
		<input type="submit" name="submit">	
	</div>
</form>


</body>
</html>