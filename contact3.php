<?php require __DIR__ . '/vendor/autoload.php' ?>
<?php 

use Symfony\Component\HttpFoundation\Session\Session;

$session = new \Symfony\Component\HttpFoundation\Session\Session();
$session->start();

if (isset($_POST['submit'])) {
	// send email
	$session->getFlashBag()->add('contact-success', 'We will get back to you shortly.');
	$session->getFlashBag()->add('contact-success', 'Thank you!');
	header('Location: contact3.php');
	exit;
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Contact Us</title>
</head>
<body>
	
<h1>Contact Us</h1>

<?php foreach ($session->getFlashBag()->get('contact-success') as $message) : ?>
	<p><?php echo $message ?></p>
<?php endforeach; ?>

<form action="contact3.php" method="post">
	<div>
		Email: <input type="text" name="email">	
	</div>
	<div>
		<textarea name="message" cols="30" rows="10"></textarea>	
	</div>
	<div>
		<input type="submit" name="submit">	
	</div>
</form>


</body>
</html>